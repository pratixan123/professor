#! /usr/bin/env python3

import numpy as np

def f(x, y):
    return 0.5 + 0.3*x + 0.8*y + 0.1*x**2 + 0.4*x*y + 0.2*y**2 + 1e-2*np.sin(x*y)

def mkext(x, y):
    #return [1, x, y, x*x, x*y, y*y]
    xx = np.array(x).reshape(-1, 1)
    yy = np.array(y).reshape(-1, 1)
    aa = np.ones(yy.shape)
    rtn = np.hstack((aa, xx, yy, xx*xx, xx*yy, yy*yy))
    #print(xx, yy, rtn, sep="\n")
    return rtn

## Print for debug
#mkext([1,2,3], [4,5,6])
#print()

## Sample a lot of 2D param points, transform into "MC values" and extended param vectors
NSAMP = 10000
randxy = np.random.rand(NSAMP, 2)
randvals = f(randxy[:,0], randxy[:,1])
randext = mkext(randxy[:,0], randxy[:,1])

## Invert to solve the coeffs: Pexts C = Vs -> C = Pexts^-1 Vs
randextinv = np.linalg.pinv(randext)
coeffs = randextinv @ randvals
print(coeffs)
