#! /usr/bin/env bash

set -e

function xdocker { echo "docker $@"; docker "$@"; }

#PLATFLAGS="--platform linux/amd64,linux/arm64"
#BUILD="xdocker buildx build -f Dockerfile $PLATFLAGS $DOCKERFLAGS ."
#if [[ -n "$PLATFLAGS" && "$PUSH" = 1 ]]; then BUILD="$BUILD --push"; fi

BUILD="xdocker build . -f Dockerfile $DOCKERFLAGS"

test "$FORCE" && BUILD="$BUILD --no-cache"

PROF_VERSION=2.4.1
ARGFLAGS="--build-arg PROF_VERSION=$PROF_VERSION"
TAGFLAGS="-t hepstore/prof2-tutorial:$PROF_VERSION"
if [[ "$LATEST" = 1 ]]; then TAGFLAGS="$TAGFLAGS -t hepstore/prof2-tutorial:latest"; fi

echo "Building Professor prof2-tutorial:$PROF_VERSION image from base prof2-pythia:$PROF_VERSION"
$BUILD $ARGFLAGS $TAGFLAGS

if [[ "$PUSH" = 1 ]]; then
    docker push -a hepstore/prof2-tutorial
fi
