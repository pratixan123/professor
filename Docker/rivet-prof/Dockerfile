FROM hepstore/rivet:latest
MAINTAINER Andy Buckley <andy.buckley@cern.ch>

ARG PROF_VERSION=2.4.0

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update -y \
    && apt-get install -y python3-venv python3-pip \
    && python -m venv /app \
    && . /app/bin/activate \
    && pip install iminuit emcee scikit-optimize superplot sobol \
       python3-pydoe python3-pymultinest python3-numpy python3-scipy python3-matplotlib python3-sympy

RUN export DEBIAN_FRONTEND=noninteractive \
    && wget https://github.com/JohannesBuchner/MultiNest/archive/master.zip \
    && unzip master.zip && cd MultiNest-master && cmake . \
    && make && make install

# Professor
RUN export DEBIAN_FRONTEND=noninteractive \
    && wget http://www.hepforge.org/archive/professor/Professor-${PROF_VERSION}.tar.gz \
    && tar xzf Professor-${PROF_VERSION}.tar.gz && cd Professor-${PROF_VERSION} && PREFIX=/usr/local make install \
    && rm -rf Professor-*

ENV LD_LIBRARY_PATH /usr/local/lib
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/contrib
