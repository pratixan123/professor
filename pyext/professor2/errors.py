# -*- python -*-

class StatError(Exception):
    pass

class IpolError(Exception):
    pass

class IpolIOError(Exception):
    pass

class NanError(Exception):
    pass

class NoBinsError(Exception):
    pass
