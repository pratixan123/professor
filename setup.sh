#! /usr/bin/bash

fpath=`readlink -f ${BASH_SOURCE[0]}`
PROFDIR=`dirname ${fpath}`
PYV=`python -c "from __future__ import print_function;import sys;v=sys.version_info[0:2];print('%i.%i'%(v))"`

export LD_LIBRARY_PATH=$PROFDIR/local/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$PROFDIR/local/lib/python${PYV}/site-packages:$PYTHONPATH
export PYTHONPATH=$PROFDIR/local/lib64/python${PYV}/site-packages:$PYTHONPATH
export PATH=$PROFDIR/local/bin:$PATH
export PATH=$PROFDIR/local/contrib:$PATH
